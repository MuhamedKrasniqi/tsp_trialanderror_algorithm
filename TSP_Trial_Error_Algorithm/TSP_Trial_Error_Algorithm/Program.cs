﻿using System;
using System.Collections.Generic;

namespace TSP_Trial_Error
{
    class Program
    {

        static void Main(string[] args)
        {
            int bestDistance = int.MaxValue;
            List<int> bestSolution = new List<int>();

            for (int i = 0; i < TSP.Tries; i++)
            {
                List<int> currentSolution = TSP.getRandomSolution(TSP.cities);
                int currentDistance = TSP.getPathDistance(currentSolution, TSP.Distances);
                if (currentDistance < bestDistance)
                {
                    bestDistance = currentDistance;
                    bestSolution = currentSolution;
                }
            }

            TSP.PrintSolution(bestSolution, bestDistance);
        }
    }
}
